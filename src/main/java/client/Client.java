package client;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

public class Client {

    public static void main(String[] args) {
        String url = "http://localhost:8080/example";

        // example get request
        get(url);

        // example delete request
        delete(url);

        // example post request
        post(url, new Object());
    }

















    private static void get(String url) {
        WebTarget target = ClientBuilder.newBuilder()
                .register(new LoggingFilter(true))
                .build().target(url);

        target.request().get();
    }

    private static void delete(String url) {
        WebTarget target = ClientBuilder.newBuilder()
                .register(new LoggingFilter(true))
                .build().target(url);

        target.request().delete();
    }

    private static void post(String url, Object data) {
        WebTarget target = ClientBuilder.newBuilder()
                .register(new LoggingFilter(true))
                .build().target(url);

        target.request()
                .post(Entity.entity(data, MediaType.APPLICATION_JSON));

    }


}

