package jpa;

import java.io.*;
import java.util.Collection;

import javax.persistence.*;

import jpa.model.Post;
import jpa.model.Author;

public class Dao implements Closeable {

    private EntityManager em;

    public Collection<Author> findAllAuthors() {
        // em.createQuery(...

        // solution goes here

        return null;
    }

    public Collection<Post> findPostsByAuthorName(String name) {

        // solution goes here

        return null;
    }














    private EntityManagerFactory factory;

    public Dao() {
        factory = Persistence.createEntityManagerFactory("jpa");

        em = factory.createEntityManager();
    }

    @Override
    public void close() {
        if (factory != null) factory.close();
    }
}
