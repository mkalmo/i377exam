package jpa;

public class Main {

    public static void main(String[] args) throws Exception {
        new SqlExecutor().loadSchemaFrom("schema.sql");

        try (Dao dao = new Dao()) {
            System.out.println(dao.findAllAuthors());
            System.out.println(dao.findPostsByAuthorName("Jill"));
        }
    }
}
