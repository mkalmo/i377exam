package jpa;

import java.sql.*;

import javax.persistence.*;

import org.hibernate.Session;

public class SqlExecutor {

    public void loadSchemaFrom(String fileOnClasspath) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("empty-unit");
        final String sql = FileUtil.readFileFromClasspath(fileOnClasspath);

        EntityManager em = factory.createEntityManager();

        em.getTransaction().begin();
        Session hibernateSession = em.unwrap(Session.class);
        hibernateSession.doWork((Connection con) -> {
            for (String statement : sql.split(";")) {
                con.createStatement().execute(statement);
            }
        });
        em.getTransaction().commit();

        em.close();
        factory.close();
    }
}
